data "ibm_resource_group" "resource_group" {
  name = "CONTENEDORES"
}

data "ibm_container_cluster_config" "cluster_config" {
  resource_group_id = data.ibm_resource_group.resource_group.id
  cluster_name_id   = var.cluster_id

  # Required for getting the calico configuration
  admin           = "true"
  network         = "true"
  config_dir      = "/tmp"
}

resource "null_resource" "apply-job-yaml-oracle-db" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "oracle-db.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-consultas" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-consultas.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-transacciones" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-transacciones.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-bash" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-bash.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}